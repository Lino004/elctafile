const main = {
  primary: '#FB405A',
  secondary: '#17356F',
  accent: '#FB405A',
  error: '#FB405A',
  // tertiary: '#20C9A0',
};
export default main;
