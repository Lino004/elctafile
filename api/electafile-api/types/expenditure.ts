export const ExpenditureType = [
  { label: 'Operating Expense', value: 'operating_expense' },
  {
    label: 'Contribution to Candidate or Political Committee',
    value: 'contribution_to_candidate_or_political_committee',
  },
  {
    label: 'Coordinated Party Expenditure',
    value: 'coordinated_party_expenditure',
  },
  // { label: 'Loan Repayment', value: 'loan_repayment' },
  // { label: 'Refund/Reimbursement from the Committee', value: 'refund' },
  // { label: 'Non-Monetary Gift', value: 'gift' },
  // { label: 'Independent Expenditure', value: 'independent_expenditure' },
];
