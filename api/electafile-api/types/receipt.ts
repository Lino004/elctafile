export const ReceiptType = [
  { label: 'General Contribution', value: 'general_contribution' },
  {
    label: 'Contribution to be Reimbursed',
    value: 'contribution_to_be_reimbursed',
  },
  {
    label: 'Refund / Reimbursement to the Committee',
    value: 'refund_reimbursement_to_the_committee',
  },
  { label: 'Interest', value: 'interest' },
  { label: 'Outside Source of Income', value: 'outside_source_of_income' },
  { label: 'Loan Proceed', value: 'loan_proceed' },
  { label: 'Forgiven Loan', value: 'forgiven_loan' },
  {
    label: 'Exempt Purchase Price Sales',
    value: 'exempt_purchase_price_sales',
  },
];
