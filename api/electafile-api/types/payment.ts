export enum PaymentType {
  CHECK = 'check',
  DEBIT_CARD = 'debit_card',
  CREDIT_CARD = 'credit_card',
  CASH = 'cash',
  FUNDS_TRANSFER = 'funds_transfer',
  DRAFT = 'draft',
  MONEY_ORDER = 'money_order',
  IN_KIND = 'in_kind',
}
