export const CounterpartyType = [
  { label: 'Individual', value: 'individual' },
  { label: 'Financial Institution', value: 'financial_institution' },
  { label: 'Political Committee', value: 'political_committee' },
  { label: 'Other Organization', value: 'other_organization' },
  // { label: 'Electioneering Communication Entity ', value: 'electioneering' },
  // {
  //   label: 'Independent Expenditure Entity ',
  //   value: 'independent_expenditure',
  // },
];

export const CounterpartySubType = [
  { label: 'Individual', value: 'individual' },
  { label: 'Organization', value: 'organization' },
];

export const CounterpartyCommiteeOrglevel = [
  { label: 'Federal', value: 'federal' },
  { label: 'State', value: 'state' },
  { label: 'County', value: 'county' },
  { label: 'Municipality', value: 'municipality' },
];

export enum CounterpartyKeys {
  COUNTERPARTY_TYPE = 'counterparty_type',
  FULL_NAME = 'full_name',
  ORGANIZATION_NAME = 'organization_name',

  JOB_TITLE = 'job_title',
  EMPLOYER_NAME = 'employer_name',
  PHONE = 'phone',
  EMAIL = 'email',
  COUNTERPARTY_SUBTYPE = 'counterparty_subtype',
  CANDIDATE_SPOUSE = 'is_candidate_spouse',
  ORGANIZATION_NON_FOR_PROFIT = 'is_non_for_profit_organization',
  COUNTERPARTY_COMMITTEE_NAME = 'committee_name',
  COUNTERPARTY_COMMITTEE_TYPE = 'committee_type',
  COUNTERPARTY_COMMITTEE_ORGANIZATIONAL_LEVEL = 'committee_organizaton_level',
}
