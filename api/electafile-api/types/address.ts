export enum AddressKeys {
  STREET = 'street',
  STATE = 'state',
  ZIP = 'zip',
  CITY = 'city',
  COUNTY = 'county',
}
