export interface RegisterUserResponse {
  [k: string]: any;
}

export interface RegisterUserInterface {
  (
    id: any,
    email: any,
    role: any,
    committee_type: any,
    first_name: any,
    last_name: any,
  ): Promise<RegisterUserResponse>;
}

export enum UserRole {
  ADMIN = 'admin',
  USER = 'user',
}

export enum CommitteeType {
  CANDIDATE_COMMITTEE = 'candidate-committee',
  PARTY_COMMITTEE = 'party-committee',
  POLITICAL_ACTION_COMMITTEE = 'political-action-committee',
  INDEPENDENT_EXPENDITURE_COMMITTEE = 'independent-expenditure-committee',
  NONE = '',
}

export enum CommitteeTypeBackend {
  CANDIDATE_COMMITTEE = 'candidate',
  PARTY_COMMITTEE = 'party',
  POLITICAL_ACTION_COMMITTEE = 'political-action',
  INDEPENDENT_EXPENDITURE_COMMITTEE = 'independent-expenditure',
}
