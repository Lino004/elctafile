export enum CommitteeKeys {
  COMMITTEE_NAME = 'committee_name',
  ELECTION_ID = 'election_id',
  ORGANIZATIONAL_LEVEL = 'committee_organization_level',
  DATE_ORGANIZED = 'date_organized',
  PHONE = 'phone',
  WEBSITE = 'website',
}

export enum CandidateKeys {
  FULL_NAME = 'full_name',
  PARTY_AFFILIATION_TYPE = 'party_affiliation_type',
  OFFICE_SOUGHT = 'office_sought',
  JURISDICTION = 'jurisdiction',
  NEXT_ELECTION_YEAR = 'next_election_year',
  PHONE = 'phone',
  EMAIL = 'email',
}

export enum PartyKeys {
  PARTY_NAME = 'party_name',
  PARTY_TYPE = 'party_type',
}

export enum AccountKeys {
  ACCOUNT_CODE = 'account_code',
  ACCOUNT_TYPE = 'account_type',
  ACCOUNT_NUMBER = 'account_number',
  FIN = 'financial_institution_name',
  PURPOSE = 'purpose',
}

// TODO:
// customItems
// assistantTreasurerItems
// custodianItems
export enum OfficerKeys {
  FULL_NAME = 'full_name',
  PHONE = 'phone',
  EMAIL = 'email',
}

// TODO:
// add organizationItems
export enum DetailsKeys {
  PAC_CATEGORY = 'pac_category',
  PAC_TYPE = 'pac_type',
  DEFINITION_TYPE = 'definition_type',
  MEMBER_DEFINITION = 'member_definition',
}
