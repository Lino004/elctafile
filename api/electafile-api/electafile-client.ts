import type { AxiosInstance } from 'axios';
import { Auth } from '@nuxtjs/auth-next/dist';
import { Client as ApiUnifiedClient } from '~/api/base-api/index';
// import { RegisterUserResponse } from "~/electafile-api/types";

let apiClient: ApiUnifiedClient;

function getApi(): AxiosInstance {
  if (!apiClient) {
    throw new Error(
      'API Electafile client should be initialized before access',
    );
  }

  return apiClient.axios;
}

export function init(
  baseUrl: string,
  auth: Auth,
  _asyncHeaders: () => void,
): ApiUnifiedClient {
  if (!apiClient) {
    apiClient = new ApiUnifiedClient(baseUrl, auth);

    /* apiClient.axios.interceptors.request.use(
        async request => {
            const headers = await asyncHeaders()
            request.headers = { ...request.headers, ...headers }
            return request
        },
        error => {
            return Promise.reject(error)
        },
    ) */

    return apiClient;
  }

  return apiClient;
}

// async function getMembersApiPrefix(memberId?: string) {
//     return memberId ? `/api/v2/members/${await encrypt(memberId)}` : '/api/v2'
// }
//
// export async function refreshToken(
//     refreshToken: string | undefined,
// ): Promise<any> {
//     const { data } = await getApi().post('/users/tokens/refresh', undefined, {
//         headers: { authorization: refreshToken },
//         skipAuthRefresh: true,
//         authorizationRequired: false,
//     } as UnifiedClientRequestConfig)
//     return data
// }
//

// API end-points
export async function registerUser(uData: any): Promise<any> {
  console.log('__API__ register');
  const { data: result } = await getApi().post('/auth/register', uData);
  console.log(`__API__ responce:`, result);
  return result;
}

export async function getUserData(email: string): Promise<any> {
  console.log('__API__ getUserData');
  const { data: result } = await getApi().get(`/users/email/${email}`);
  console.log(`__API__ response:`, result);
  return result;
}

export async function updateUser(userID: string, data: any): Promise<any> {
  console.log('__API__ updateUser');
  const { data: result } = await getApi().patch(`${'/users/' + userID}`, data);
  console.log(`__API__ updateUser response:`, result);
  return result;
}

export async function createCommittee(data: any): Promise<any> {
  console.log('__API__ createCommittee');
  const { data: result } = await getApi().post('/committee/create', data);
  console.log(`__API__ createCommittee response:`, result);
  return result;
}

export async function updateCommittee(
  committeeID: string,
  data: any,
): Promise<any> {
  console.log('__API__ updateCommittee');
  const { data: result } = await getApi().patch(
    `${'/committee/' + committeeID}`,
    data,
  );
  console.log(`__API__ updateCommittee response:`, result);
  return result;
}

export async function getCommittee(userID: string): Promise<any> {
  console.log('__API__ getCommittee');
  const { data: result } = await getApi().get(`${'/committee/user/' + userID}`);
  console.log(`__API__ getCommittee response:`, result);
  return result;
}

/* candidate */
export async function createCandidate(data: any): Promise<any> {
  console.log('__API__ createCandidate');
  const { data: result } = await getApi().post('/candidate/create', data);
  console.log(`__API__ createCandidate response:`, result);
  return result;
}

export async function updateCandidate(
  candidateID: string,
  data: any,
): Promise<any> {
  console.log('__API__ updateCandidate');
  const { data: result } = await getApi().patch(
    `${'/candidate/' + candidateID}`,
    data,
  );
  console.log(`__API__ updateCandidate response:`, result);
  return result;
}

export async function getCandidate(userID: string): Promise<any> {
  console.log('__API__ getCandidate');
  const { data: result } = await getApi().get(`${'/candidate/user/' + userID}`);
  console.log(`__API__ getCandidate response:`, result);
  return result;
}

/* Officer */
export async function createOfficer(data: any): Promise<any> {
  console.log('__API__ createOfficer');
  const { data: result } = await getApi().post('/officers/create', data);
  console.log(`__API__ createOfficer response:`, result);
  return result;
}

export async function updateOfficer(
  officerID: string,
  data: any,
): Promise<any> {
  console.log('__API__ updateOfficer');
  const { data: result } = await getApi().patch(
    `${'/officers/' + officerID}`,
    data,
  );
  console.log(`__API__ updateOfficer response:`, result);
  return result;
}

export async function getOfficer(userID: string): Promise<any> {
  console.log('__API__ getOfficer');
  const { data: result } = await getApi().get(`${'/officers/user/' + userID}`);
  console.log(`__API__ getOfficer response:`, result);
  return result;
}

export async function createAccount(data: any): Promise<any> {
  console.log('__API__ createAccount');
  const { data: result } = await getApi().post('/account/create', data);
  console.log(`__API__ createAccount response:`, result);
  return result;
}

export async function updateAccount(
  officerID: string,
  data: any,
): Promise<any> {
  console.log('__API__ updateAccount');
  const { data: result } = await getApi().patch(
    `${'/account/' + officerID}`,
    data,
  );
  console.log(`__API__ updateAccount response:`, result);
  return result;
}

export async function getAccount(userID: string): Promise<any> {
  console.log('__API__ getAccount');
  const { data: result } = await getApi().get(`${'/account/user/' + userID}`);
  console.log(`__API__ getAccount response:`, result);
  return result;
}

/*  Party */
export async function createParty(data: any): Promise<any> {
  console.log('__API__ createParty');
  const { data: result } = await getApi().post('/party/create', data);
  console.log(`__API__ createParty response:`, result);
  return result;
}

export async function updateParty(officerID: string, data: any): Promise<any> {
  console.log('__API__ updateParty');
  const { data: result } = await getApi().patch(
    `${'/party/' + officerID}`,
    data,
  );
  console.log(`__API__ updateAccount response:`, result);
  return result;
}

export async function getParty(userID: string): Promise<any> {
  console.log('__API__ getParty');
  const { data: result } = await getApi().get(`${'/party/user/' + userID}`);
  console.log(`__API__ getParty response:`, result);
  return result;
}

/*  Details */
export async function createDetails(data: any): Promise<any> {
  console.log('__API__ createDetails');
  const { data: result } = await getApi().post('/details/create', data);
  console.log(`__API__ createDetails response:`, result);
  return result;
}

export async function updateDetails(
  officerID: string,
  data: any,
): Promise<any> {
  console.log('__API__ updateDetails');
  const { data: result } = await getApi().patch(
    `${'/details/' + officerID}`,
    data,
  );
  console.log(`__API__ updateDetails response:`, result);
  return result;
}

export async function getDetails(userID: string): Promise<any> {
  console.log('__API__ getDetails');
  const { data: result } = await getApi().get(`${'/details/user/' + userID}`);
  console.log(`__API__ getDetails response:`, result);
  return result;
}

// Expenditures
export async function createExpenditure(data: any): Promise<any> {
  console.log('__API__ createExpenditure');
  const { data: result } = await getApi().post('/expenditures/create', data);
  console.log(`__API__ createExpenditure response:`, result);
  return result;
}

export async function updateExpenditure(
  expenditureID: string,
  data: any,
): Promise<any> {
  console.log('__API__ updateExpenditure');
  const { data: result } = await getApi().patch(
    `${'/expenditures/' + expenditureID}`,
    data,
  );
  console.log(`__API__ updateExpenditure response:`, result);
  return result;
}

export async function getExpenditures(userID: string): Promise<any> {
  console.log('__API__ getExpenditures');
  const { data: result } = await getApi().get(
    `${'/expenditures/user/' + userID}`,
  );
  console.log(`__API__ get getExpenditures response:`, result);
  return result;
}

export async function getExpenditure(expenditureID: string): Promise<any> {
  console.log('__API__ getExpenditure');
  const { data: result } = await getApi().get(
    `${'/expenditures/' + expenditureID}`,
  );
  console.log(`__API__ get getExpenditure response:`, result);
  return result;
}

// TODO:
// getExpenditures(expenditureID: string)
// deletetExpenditure

// Counterparties
export async function createCounterparty(data: any): Promise<any> {
  console.log('__API__ createCounterparty');
  const { data: result } = await getApi().post('/counterparties/create', data);
  console.log(`__API__ createCounterparty response:`, result);
  return result;
}

export async function updateCounterparty(
  counterpartyID: string,
  data: any,
): Promise<any> {
  console.log('__API__ updateCounterparty');
  const { data: result } = await getApi().patch(
    `${'/counterparties/' + counterpartyID}`,
    data,
  );
  console.log(`__API__ updateCounterparty response:`, result);
  return result;
}

export async function getCounterparties(userID: string): Promise<any> {
  console.log('__API__ getCounterparties');
  const { data: result } = await getApi().get(
    `${'/counterparties/user/' + userID}`,
  );
  console.log(`__API__ get getCounterparties response:`, result);
  return result;
}

export async function getCounterparty(counterpartyID: string): Promise<any> {
  console.log('__API__ getCounterparty');
  const { data: result } = await getApi().get(
    `${'/counterparties/' + counterpartyID}`,
  );
  console.log(`__API__ get getCounterparty response:`, result);
  return result;
}

// Receipts
export const apiReceipt = {
  create: async (data: any): Promise<any> => {
    console.log('__API__ apiReceipt.create');
    const { data: result } = await getApi().post('/receipts/create', data);
    console.log(`__API__ apiReceipt.create response:`, result);
    return result;
  },
  get: async (receiptID: string): Promise<any> => {
    console.log('__API__ apiReceipt.get');
    const { data: result } = await getApi().get(`/receipts/${receiptID}`);
    console.log('__API__ apiReceipt.get response');
    return result;
  },
  getAll: async (userID: string): Promise<any> => {
    console.log('__API__ apiReceipt.getAll');
    const { data: result } = await getApi().get(
      `${'/receipts/user/' + userID}`,
    );
    console.log('__API__ apiReceipt.getAll response');
    return result;
  },
  update: async (data: any, receiptID: string): Promise<any> => {
    console.log('__API__ apiReceipt.update');
    const { data: result } = await getApi().patch(
      `/receipts/${receiptID}`,
      data,
    );
    console.log(`__API__ apiReceipt.update response:`, result);
    return result;
  },
  delete: async (receiptID: string): Promise<any> => {
    console.log('__API__ apiReceipt.delete');
    const { data: result } = await getApi().delete(`/receipts/${receiptID}`);
    console.log('__API__ apiReceipt.delete response');
    return result;
  },
};
