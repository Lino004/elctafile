import * as apiElectafile from '~/api/electafile-api';
import { accessor } from '~/store';

class DetailsService {
  public async createDetails(data: any): Promise<any> {
    console.log('createDetails');
    return await apiElectafile.createDetails(data);
  }

  public async updateDetails(detailsID: string, data: any): Promise<any> {
    console.log('updateDetails');
    return await apiElectafile.updateDetails(detailsID, data);
  }

  public async getDetails(userID: string): Promise<any> {
    console.log('getDetails');
    return await apiElectafile.getDetails(userID);
  }
}
export const detailsService = new DetailsService();
