import * as apiElectafile from '~/api/electafile-api';

class CommitteeService {
  public async createCommittee(data: any): Promise<any> {
    console.log('createCommittee');
    return await apiElectafile.createCommittee(data);
  }

  public async updateCommittee(committeeID: string, data: any): Promise<any> {
    console.log('updateCommittee');
    return await apiElectafile.updateCommittee(committeeID, data);
  }

  public async getCommittee(userID: string): Promise<any> {
    console.log('getCommittee');
    return await apiElectafile.getCommittee(userID);
  }
}
export const committeeService = new CommitteeService();
