import * as apiElectafile from '~/api/electafile-api';

class ExpenditureService {
  public async createExpenditure(data: any): Promise<any> {
    console.log('createExpenditure');
    return await apiElectafile.createExpenditure(data);
  }

  public async updateExpenditure(
    expenditureID: string,
    data: any,
  ): Promise<any> {
    console.log('updateExpenditure');
    return await apiElectafile.updateExpenditure(expenditureID, data);
  }

  public async getExpenditures(userID: string): Promise<any> {
    console.log('getExpenditures');
    return await apiElectafile.getExpenditures(userID);
  }

  public async getExpenditure(expenditureID: string): Promise<any> {
    console.log('getExpenditure');
    return await apiElectafile.getExpenditure(expenditureID);
  }
}
export const expenditureService = new ExpenditureService();
