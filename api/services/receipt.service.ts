import { apiReceipt } from '~/api/electafile-api';

class ReceiptService {
  public async getReceipts(userID: string): Promise<any> {
    return await apiReceipt.getAll(userID);
  }

  public async getReceipt(id: string): Promise<any> {
    return await apiReceipt.get(id);
  }

  public async createReceipt(data: any): Promise<any> {
    return await apiReceipt.create(data);
  }

  public async updateReceipt(payload: any): Promise<any> {
    return await apiReceipt.update(payload.data, payload.id);
  }

  public async deleteReceipt(id: string): Promise<any> {
    return await apiReceipt.delete(id);
  }
}

export const receiptService = new ReceiptService();
