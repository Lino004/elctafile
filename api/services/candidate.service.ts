import * as apiElectafile from '~/api/electafile-api';
import { accessor } from '~/store';

class CandidateService {
  public async createCandidate(data: any): Promise<any> {
    console.log('createCandidate');
    return await apiElectafile.createCandidate(data);
  }

  public async updateCandidate(candidateID: string, data: any): Promise<any> {
    console.log('updateCandidate');
    return await apiElectafile.updateCandidate(candidateID, data);
  }

  public async getCandidate(userID: string): Promise<any> {
    console.log('getCandidate');
    return await apiElectafile.getCandidate(userID);
  }
}
export const candidateService = new CandidateService();
