import * as apiElectafile from '~/api/electafile-api';
import { accessor } from '~/store';

class PartyService {
  public async createParty(data: any): Promise<any> {
    console.log('createParty');
    return await apiElectafile.createParty(data);
  }

  public async updateParty(partyID: string, data: any): Promise<any> {
    console.log('updateParty');
    return await apiElectafile.updateParty(partyID, data);
  }

  public async getParty(userID: string): Promise<any> {
    console.log('getCandidate');
    return await apiElectafile.getParty(userID);
  }
}
export const partyService = new PartyService();
