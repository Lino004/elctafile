import * as apiElectafile from '~/api/electafile-api';
import { accessor } from '~/store';

class CounterpartyService {
  public async createCounterparty(data: any): Promise<any> {
    console.log('createCounterparty');
    return await apiElectafile.createCounterparty(data);
  }

  public async updateCounterparty(
    counterpartyID: string,
    data: any,
  ): Promise<any> {
    console.log('updateCounterparty');
    return await apiElectafile.updateCounterparty(counterpartyID, data);
  }

  public async getCounterparties(userID: string): Promise<any> {
    console.log('getCounterparties');
    return await apiElectafile.getCounterparties(userID);
  }

  public async getCounterparty(counterpartyID: string): Promise<any> {
    console.log('getCounterparty');
    return await apiElectafile.getCounterparty(counterpartyID);
  }
}
export const counterpartyService = new CounterpartyService();
