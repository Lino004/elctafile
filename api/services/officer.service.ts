import * as apiElectafile from '~/api/electafile-api';

class OfficerService {
  public async createOfficer(data: any): Promise<any> {
    console.log('createOfficer');
    return await apiElectafile.createOfficer(data);
  }

  public async updateOfficer(officerID: string, data: any): Promise<any> {
    console.log('updateOfficer');
    return await apiElectafile.updateOfficer(officerID, data);
  }

  public async getOfficer(userID: string): Promise<any> {
    console.log('getOfficer');
    return await apiElectafile.getOfficer(userID);
  }
}
export const officerService = new OfficerService();
