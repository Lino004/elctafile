import * as apiElectafile from '~/api/electafile-api';

class AccountService {
  public async createAccount(data: any): Promise<any> {
    console.log('createAccount');
    return await apiElectafile.createAccount(data);
  }
  public async updateAccount(accountID: string, data: any): Promise<any> {
    console.log('updateAccount');
    return await apiElectafile.updateAccount(accountID, data);
  }

  public async getAccount(userID: string): Promise<any> {
    console.log('getAccount');
    return await apiElectafile.getAccount(userID);
  }
}
export const accountService = new AccountService();
