import * as apiElectafile from '~/api/electafile-api/index';
import api from '~/store/module/api';
// TODO:
//  .env
// types
// repsonce types

class UserService {
  public async registration(uData: any): Promise<any> {
    return await apiElectafile.registerUser(uData);
  }

  public async getUserData(email: string): Promise<any> {
    const data = await apiElectafile.getUserData(email);
    return data;
  }

  public async updateUserData(userID: string, data: any): Promise<any> {
    const result = await apiElectafile.updateUser(userID, data);
    return result;
  }
}
export const userService = new UserService();
