/**
 * Types to be exported in root namespace of a package.
 * Components and other entities should be exported under specific namespace.
 */

export { default as Client } from './client';
export type { UnifiedClientRequestConfig } from './client';
