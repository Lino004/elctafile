import type { AxiosInstance } from 'axios';
import axios from 'axios';
import { Auth } from '@nuxtjs/auth-next/dist';

import type {
  AxiosAuthRefreshOptions,
  AxiosAuthRefreshRequestConfig,
} from 'axios-auth-refresh';

import createAuthRefreshInterceptor from 'axios-auth-refresh';

export interface UnifiedClientRequestConfig
  extends AxiosAuthRefreshRequestConfig {
  authorizationRequired?: boolean;
}

export default class Client {
  protected readonly instance: AxiosInstance;

  public get axios(): AxiosInstance {
    return this.instance;
  }

  constructor(
    baseURL: string,
    auth: Auth,
    axiosRefreshOptions: AxiosAuthRefreshOptions = {
      statusCodes: [401],
      pauseInstanceWhileRefreshing: true,
    },
  ) {
    const config: UnifiedClientRequestConfig = {
      baseURL,
      authorizationRequired: true,
    };

    this.instance = axios.create(config);

    // createAuthRefreshInterceptor(
    //     this.instance,
    //     //tokenProvider.refresh,
    //     //auth.strategy.refreshToken.sync(),
    //     auth.strategy.refreshToken.get(),
    //     axiosRefreshOptions,
    // )

    this.instance.interceptors.request.use(async (requestConfig) => {
      const request = requestConfig as UnifiedClientRequestConfig;

      if (!request.authorizationRequired) {
        return request;
      }

      // @ts-ignore
      const token = auth.strategy.token.get();
      //console.log(">> get token:", token)
      request.headers = {
        ...request.headers,
        ...{ Authorization: token },
      };

      return request;
    });
  }
}
