// @ts-ignore
module.exports = {
  extends: ['@nuxtjs/eslint-config-typescript', 'prettier'],
  plugins: ['prettier'],
  rules: {
    'prettier/prettier': ['error'],
    'no-console': 'off',
  },
  ignorePatterns: ['~/assets/*'],
};
