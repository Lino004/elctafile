module.exports = {
  '*.vue': ['eslint --fix', 'prettier --write'],
  '*.js': ['eslint --fix', 'prettier --write'],
  '*.md': ['prettier --write'],
};
