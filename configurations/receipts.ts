import moment from 'moment';
import { ReceiptType } from './../api/electafile-api/types/receipt';

export interface ItemReceipt {
  id: number;
  type: string;
  label: string;
  propriety: string;
  value: any;
}

const DEFAULT_RECEIPT_CREATE = {
  payment_type: '',
  payment_info: {
    account: '',
  },
  amount: 0,
  counterparty_id: '',
  counterparty_name: '',
  comments: '',
  user_id: '',
  cycle_id: '',
  entry_date: '',
  receipt_type: '',
};

export function getItemsReceiptFrom(
  CONTRIBUTOR: any,
  PAYEE: any,
  EXPENDITURES: any,
  LOAN_FORGIVEN: any,
  PLAN: any,
  EFConditionalButton: any,
  receipt: any,
) {
  // General Contribution
  const items0 = [
    {
      id: 0,
      type: 'amount',
      label: 'Amount',
      placeholder: '0.00',
      propriety: 'amount',
      value: '',
    },
    {
      id: 1,
      type: 'select',
      variant: 'datepicker',
      label: 'Date',
      rules: [(v: any) => !!v || 'Date is required'],
      propriety: 'entry_date',
      value: '',
    },
    {
      id: 2,
      type: 'select',
      label: 'Contributor',
      rules: [(v: any) => !!v || 'Contributor is required'],
      options: CONTRIBUTOR,
      propriety: 'counterparty',
      value: {},
    },
    {
      id: 3,
      type: 'textarea',
      label: 'Comments',
      counter: 70,
      labelRight: '(optional)',
      propriety: 'comments',
      value: '',
    },
  ];
  // Contribution to be Reimbursed
  const items1 = [
    {
      id: 0,
      type: 'amount',
      label: 'Amount',
      placeholder: '0.00',
      propriety: '',
      cols: 6,
    },
    {
      id: 1,
      propriety: '',
      value: '',
      type: 'text',
      label: 'Contribution Description',
      counter: 70,
      cols: 6,
    },
    {
      id: 2,
      propriety: '',
      value: '',
      type: 'select',
      variant: 'datepicker',
      label: 'Date',
      rules: [(v: any) => !!v || 'Date is required'],
      cols: 6,
    },
    {
      id: 3,
      propriety: '',
      value: '',
      type: 'customItems',
      label: 'Credit Card Purchaise',
      content: EFConditionalButton,
      cols: 6,
    },
    {
      id: 4,
      propriety: '',
      value: '',
      type: 'select',
      label: 'Payee',
      rules: [(v: any) => !!v || 'Payee is required'],
      hint: 'The original vendor',
      cols: 7,
      class: 'mr-16 pr-1',
      options: PAYEE,
    },
    {
      id: 5,
      propriety: '',
      value: '',
      type: 'select',
      label: 'Reimbursee',
      rules: [(v: any) => !!v || 'Reimbursee is required'],
      hint: 'The person to whom the campaign check is written',
      cols: 6,
      options: ['Option 1', 'Option 2'],
    },
  ];
  // Refund / Reimbursement to the Committee
  const items2 = [
    {
      id: 0,
      propriety: '',
      value: '',
      type: 'select',
      label: 'Expenditure Refunded/Reimbursed',
      rules: [(v: any) => !!v || 'Expenditure Refunded/Reimbursed is required'],
      options: EXPENDITURES,
    },
    {
      id: 1,
      propriety: '',
      value: '',
      type: 'amount',
      label: 'Amount Refunded/Reimbursed',
      placeholder: '0.00',
    },
    {
      id: 2,
      propriety: '',
      value: '',
      type: 'select',
      variant: 'datepicker',
      label: 'Refund/Reimbursement Date',
      rules: [(v: any) => !!v || 'Refund/Reimbursement Date is required'],
    },
    {
      id: 3,
      propriety: '',
      value: '',
      type: 'text',
      label: 'Refund/Reimbursement Purpose',
      counter: 25,
    },
    {
      id: 4,
      propriety: '',
      value: '',
      type: 'textarea',
      label: 'Comments',
      counter: 70,
      labelRight: '(optional)',
    },
  ];
  // Interest
  const items3 = [
    {
      id: 0,
      propriety: '',
      value: '',
      type: 'amount',
      label: 'Amount',
      placeholder: '0.00',
    },
    {
      id: 1,
      propriety: '',
      value: '',
      type: 'select',
      variant: 'datepicker',
      label: 'Date',
      rules: [(v: any) => !!v || 'Date is required'],
    },
    {
      id: 2,
      propriety: '',
      value: '',
      type: 'select',
      label: 'Contributor',
      rules: [(v: any) => !!v || 'Contributor is required'],
      options: CONTRIBUTOR,
    },
    {
      id: 3,
      propriety: '',
      value: '',
      type: 'textarea',
      label: 'Comments',
      counter: 70,
      labelRight: '(optional)',
    },
  ];
  // Outside Source of Income
  const items4 = [
    {
      id: 0,
      propriety: '',
      value: '',
      type: 'amount',
      label: 'Amount',
      placeholder: '0.00',
    },
    {
      id: 1,
      propriety: '',
      value: '',
      type: 'select',
      variant: 'datepicker',
      label: 'Date',
      rules: [(v: any) => !!v || 'Date is required'],
    },
    {
      id: 2,
      propriety: '',
      value: '',
      type: 'select',
      label: 'Contributor',
      rules: [(v: any) => !!v || 'Contributor is required'],
      options: CONTRIBUTOR,
    },
    {
      id: 3,
      propriety: '',
      value: '',
      type: 'text',
      label: 'Outside Source Explanation',
      counter: 70,
    },
    {
      id: 4,
      propriety: '',
      value: '',
      type: 'textarea',
      label: 'Comments',
      counter: 70,
      labelRight: '(optional)',
    },
  ];
  // Loan Proceed
  const items5 = [
    {
      id: 0,
      propriety: '',
      value: '',
      type: 'amount',
      label: 'Amount',
      placeholder: '0.00',
    },
    {
      id: 1,
      propriety: '',
      value: '',
      type: 'select',
      variant: 'datepicker',
      label: 'Start Date',
      rules: [(v: any) => !!v || 'Start date is required'],
      cols: 6,
    },
    {
      id: 2,
      propriety: '',
      value: '',
      type: 'select',
      variant: 'datepicker',
      label: 'End Date',
      rules: [(v: any) => !!v || 'End date is required'],
      cols: 6,
    },
    {
      id: 3,
      propriety: '',
      value: '',
      type: 'percentage',
      label: 'Rate',
      cols: 4,
    },
    {
      id: 4,
      propriety: '',
      value: '',
      type: 'text',
      label: 'Security Pledged',
      cols: 8,
    },
    {
      id: 5,
      propriety: '',
      value: '',
      type: 'select',
      label: 'Lender',
      rules: [(v: any) => !!v || 'Lender is required'],
      options: ['Option 1', 'Option 2'],
    },
    {
      id: 6,
      propriety: '',
      value: '',
      type: 'text',
      label: 'Full Name of Lending Institution',
      cols: 8,
    },
    {
      id: 7,
      propriety: '',
      value: '',
      type: 'text',
      label: 'Loan Number',
      cols: 4,
    },
    {
      id: 8,
      propriety: '',
      value: '',
      type: 'textarea',
      label: 'Comments',
      counter: 70,
      labelRight: '(optional)',
    },
  ];
  // Forgiven Loan
  const items6 = [
    {
      id: 0,
      propriety: '',
      value: '',
      type: 'select',
      label: 'Loan That Was Forgiven',
      rules: [(v: any) => !!v || 'Loan that was forgiven is required'],
      options: LOAN_FORGIVEN,
      cols: 6,
    },
    {
      id: 1,
      propriety: '',
      value: '',
      type: 'textarea',
      label: 'Comments',
      counter: 70,
      labelRight: '(optional)',
      cols: 6,
    },
    {
      id: 2,
      propriety: '',
      value: '',
      type: 'amount',
      label: 'Amount Forgiven',
      placeholder: '0.00',
      cols: 6,
    },
    {
      cols: 6,
    },
    {
      id: 3,
      propriety: '',
      value: '',
      type: 'select',
      variant: 'datepicker',
      label: 'Date When Loan Was Forgiven',
      rules: [(v: any) => !!v || 'Date is required'],
      cols: 6,
    },
  ];
  // Exempt Purchase Price Sales
  const items7 = [
    {
      id: 0,
      propriety: '',
      value: '',
      type: 'amount',
      label: 'Amount',
      placeholder: '0.00',
      cols: 6,
    },
    {
      id: 1,
      propriety: '',
      value: '',
      type: 'customItems',
      label:
        'Was this sale conducted according to the Exempt Sales Plan approved by the State Board of Elections?',
      content: EFConditionalButton,
      cols: 6,
    },
    {
      id: 2,
      propriety: '',
      value: '',
      type: 'select',
      variant: 'picker',
      label: 'Plan',
      rules: [(v: any) => !!v || 'Plan is required'],
      options: PLAN,
      cols: 6,
    },
    {
      id: 3,
      propriety: '',
      value: '',
      type: 'textarea',
      label: 'Comments',
      counter: 70,
      labelRight: '(optional)',
      cols: 6,
    },
    {
      id: 4,
      propriety: '',
      value: '',
      type: 'number',
      label: 'Number of Items Sold',
      cols: 6,
      class: 'pr-16',
    },
    {
      id: 5,
      propriety: '',
      value: '',
      type: 'customItems',
      label: 'Did any purchaser make total purchases exceeding $50?',
      content: EFConditionalButton,
      cols: 6,
    },
    {
      cols: 6,
    },
    {
      id: 6,
      propriety: '',
      value: '',
      type: 'textarea',
      label: 'Comments',
      counter: 70,
      labelRight: '(optional)',
      cols: 6,
      class: '',
      // style: 'margin-left: 440px'
    },
  ];

  const type = [
    {
      label: '',
      value: '',
      items: items0,
    },
    {
      label: '',
      value: '',
      items: items1,
    },
    {
      label: '',
      value: '',
      items: items2,
    },
    {
      label: '',
      value: '',
      items: items3,
    },
    {
      label: '',
      value: '',
      items: items4,
    },
    {
      label: '',
      value: '',
      items: items5,
    },
    {
      label: '',
      value: '',
      items: items6,
    },
    {
      label: '',
      value: '',
      items: items7,
    },
  ];

  type.forEach((el, index) => {
    el.label = ReceiptType[index].label;
    el.value = ReceiptType[index].value;
  });

  if (receipt) {
    console.log('❗ . file: receipts.ts . line 438 . receipt', receipt);
    const index = type.findIndex((el) => el.value === receipt.receipt_type);
    if (index !== -1) {
      type[index].items.forEach((item: any) => {
        if (item.propriety === 'amount') {
          item.value = receipt.amount;
        }
        if (item.propriety === 'entry_date') {
          const date = moment(receipt.entry_date).format('YYYY-MM-DD');
          item.value = date;
          item.selected = date;
          item.data = date;
          item.variant = 'datepicker';
        }
        if (item.propriety === 'comments') {
          item.value = receipt.comments;
        }
        if (item.propriety === 'counterparty') {
          const data = {
            label: receipt.counterparty_name,
            value: {
              cycleId: receipt.cycle_id,
              counterpartyId: receipt.counterparty_id,
            },
          };
          item.value = data;
          item.selected = data;
        }
      });
    }
  }

  return type;
}

export function getReceiptForSave(
  items: Array<ItemReceipt>,
  paymentData: any,
  receiptType: string,
  errorMessages: Array<String>,
) {
  const data = { ...DEFAULT_RECEIPT_CREATE };
  if (receiptType) data.receipt_type = receiptType;
  items.forEach((item) => {
    if (item.propriety === 'amount') {
      if (item.value) data.amount = item.value;
      else errorMessages.push('incorrect "amount" field');
    }
    if (item.propriety === 'entry_date') {
      if (item.value) data.entry_date = item.value;
      else errorMessages.push('incorrect "entry_date" field');
    }
    if (item.propriety === 'comments') {
      if (item.value) data.comments = item.value;
    }
    if (item.propriety === 'counterparty') {
      if (item.value && item.value.value && item.value.label) {
        data.cycle_id = item.value.value.cycleId;
        data.counterparty_id = item.value.value.counterpartyId;
        data.counterparty_name = item.value.label;
      } else errorMessages.push('incorrect "contributor" field');
    }
  });
  if (paymentData && paymentData.value && paymentData.key) {
    data.payment_type = paymentData.key;
    data.payment_info = {
      account: paymentData.value,
    };
  } else errorMessages.push('incorrect "payment" field');
  return data;
}
