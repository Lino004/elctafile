import { Auth } from '@nuxtjs/auth-next/dist/runtime';
import * as apiElectafile from '~/api/electafile-api';

export default {
  init(auth: Auth) {
    // @ts-ignore
    apiElectafile.init(window.$nuxt.$config.NUXT_ENV_API_BASE_URL, auth, () => {
      Promise.resolve({ success: true });
    });
  },
};
