import { userService } from '~/api/services/user.service';
import { actionTree, mutationTree } from 'typed-vuex/lib';
import { CommitteeType } from '~/api/electafile-api/types/user';
import { accessor } from '~/store';
import { accountService } from '~/api/services/account.service';

export interface UserState {
  userID: string;
  authID: string;
  name: string;
  email: string;
  role: string;
  is_logged_in: boolean;
  committee_type: CommitteeType;
  is_committee_completed: boolean;
}

export const namespaced = true;
export const state = (): UserState => ({
  userID: '',
  authID: '',
  name: '',
  email: '',
  role: '',
  is_logged_in: false,
  committee_type: CommitteeType.NONE,
  is_committee_completed: false,
});

export const mutations = mutationTree(state, {
  SET_USER_ID(state: UserState, userID: string) {
    state.userID = userID;
  },
  SET_AUTH0_ID(state: UserState, authID: string) {
    state.authID = authID;
  },
  SET_NAME(state: UserState, name: string) {
    state.name = name;
  },
  SET_ROLE(state: UserState, role: string) {
    state.role = role;
  },
  SET_EMAIL(state: UserState, email: string) {
    state.email = email;
  },
  SET_COMMITTEE_TYPE(state: UserState, committeeType: CommitteeType) {
    state.committee_type = committeeType;
  },
  SET_COMMITTEE_COMPLETED(state: UserState, is_committee_completed: boolean) {
    state.is_committee_completed = is_committee_completed;
  },
  SET_LOGGED_IN(state: UserState, value: boolean) {
    state.is_logged_in = value;
  },
});

export const actions = actionTree(
  { state, mutations },
  {
    async login({ commit }, email) {
      commit('SET_LOGGED_IN', true);
      commit('SET_EMAIL', email);
    },

    async register({ commit }, uData: any) {
      try {
        console.log('__STORE__ register');
        const data = await userService.registration(uData);
        console.log(`response allocatedCompanies: `, data);

        commit('SET_USER_ID', data.id);
        commit('SET_AUTH0_ID', data.authID);
        commit('SET_COMMITTEE_TYPE', data.committee_type);
        commit('SET_COMMITTEE_COMPLETED', data.is_committee_completed);
        commit('SET_ROLE', data.role);

        return data;
      } catch (e) {
        throw e;
      }
    },

    async cleanData({ commit }) {
      commit('SET_LOGGED_IN', false);
      commit('SET_EMAIL', '');
      commit('SET_USER_ID', '');
      commit('SET_AUTH0_ID', '');
      commit('SET_COMMITTEE_TYPE', CommitteeType.NONE);
      commit('SET_COMMITTEE_COMPLETED', false);
      commit('SET_ROLE', '');
    },

    async getUserData({ commit }, email) {
      console.log('__STORE__ getUserData: ', email);
      try {
        const data = await userService.getUserData(email);

        commit('SET_USER_ID', data.id);
        commit('SET_AUTH0_ID', data.authID);
        commit('SET_COMMITTEE_TYPE', data.committee_type);
        commit('SET_COMMITTEE_COMPLETED', data.is_committee_completed);
        commit('SET_ROLE', data.role);

        // "createdAt": "2021-10-01T21:15:54.906Z",
        // "firstName": "",
        // "lastName": "",

        return data;
      } catch (e) {
        throw e;
      }
    },

    async completeCommittee({ commit }): Promise<any> {
      try {
        await accessor.restored;
        let aData: any = {
          is_committee_completed: true,
        };
        const data = await userService.updateUserData(
          accessor.user.userID,
          aData,
        );
        console.log('>>>> response update data:', data);
        commit('SET_COMMITTEE_COMPLETED', true);
        return data;
      } catch (e) {
        throw e;
      }
    },
  },
);
