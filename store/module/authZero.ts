import { Auth } from '@nuxtjs/auth-next/dist';
import api from '~/store/module/api';

export default {
  init(auth: Auth) {
    // @ts-ignore
    api.init(auth);
  },

  async login(data: any, auth: Auth): Promise<any> {
    const email = data.email;
    const password = data.password;

    return await auth.request({
      method: 'post',
      // @ts-ignore
      url: `https://${window.$nuxt.$config.NUXT_ENV_AUTHENTICATION_AUTH0_DOMAIN}/oauth/token`,
      baseURL: '',
      data: {
        username: email,
        password,
        scope: 'openid profile email',
        // @ts-ignore
        client_id: window.$nuxt.$config.NUXT_ENV_AUTHENTICATION_AUTH0_CLIENT_ID,
        // @ts-ignore
        audience: window.$nuxt.$config.NUXT_ENV_AUTHENTICATION_AUTH0_AUDIENCE,
        grant_type: 'password',
      },
    });
  },

  async register(data: any, auth: Auth): Promise<any> {
    return await auth.request({
      method: 'post',
      // @ts-ignore
      url: `https://${window.$nuxt.$config.NUXT_ENV_AUTHENTICATION_AUTH0_DOMAIN}/dbconnections/signup`,
      baseURL: '',
      data: {
        email: data.email,
        password: data.password,
        connection: 'Username-Password-Authentication',
        // @ts-ignore
        client_id: window.$nuxt.$config.NUXT_ENV_AUTHENTICATION_AUTH0_CLIENT_ID,
      },
    });
  },
};
