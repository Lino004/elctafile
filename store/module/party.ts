import { accessor } from '~/store';
import { actionTree, mutationTree } from 'typed-vuex/lib';
import { partyService } from '~/api/services/party.service';
import { formUtils } from '~/utils/common/forms';

export interface PartyState {
  partyID: string;
}

export const namespaced = true;
export const state = (): PartyState => ({
  partyID: '',
});

export const mutations = mutationTree(state, {
  SET_PARTY_ID(state: PartyState, partyID: string) {
    state.partyID = partyID;
  },
});

export const actions = actionTree(
  { state, mutations },
  {
    async cleanData({ commit }) {
      commit('SET_PARTY_ID', '');
    },

    async createParty({ commit }, items: any): Promise<any> {
      let cData: any = formUtils.buildParams(items);
      cData.user_id = accessor.user.userID;
      console.log('__STORE__ cData: ', cData);
      try {
        console.log('__STORE__ create/update Party');
        if (accessor.party.partyID) {
          const data = await partyService.updateParty(
            accessor.party.partyID,
            cData,
          );
          console.log('>>>> response update data:', data);
          return data;
        } else {
          const data = await partyService.createParty(cData);
          commit('SET_PARTY_ID', data.id);
          console.log('>>>> response data:', data);
          return data;
        }
      } catch (e) {
        console.log('candidate> response error:', e);
        throw e;
      }
    },

    async getParty({ commit }): Promise<any> {
      try {
        console.log('__STORE__ getParty');
        let data = await partyService.getParty(accessor.user.userID);
        commit('SET_PARTY_ID', data.id);
        console.log('>>>> response data:', data);
        return data;
      } catch (e) {
        throw e;
      }
    },
  },
);
