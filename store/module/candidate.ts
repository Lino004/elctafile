import { candidateService } from '~/api/services/candidate.service';
import { accessor } from '~/store';
import { actionTree, mutationTree } from 'typed-vuex/lib';
import { formUtils } from '~/utils/common/forms';

export interface CandidateState {
  candidateID: string;
}

export const namespaced = true;
export const state = (): CandidateState => ({
  candidateID: '',
});

export const mutations = mutationTree(state, {
  SET_CANDIDATE_ID(state: CandidateState, candidateID: string) {
    state.candidateID = candidateID;
  },
});

export const actions = actionTree(
  { state, mutations },
  {
    async cleanData({ commit }) {
      commit('SET_CANDIDATE_ID', '');
    },

    async createCandidate({ commit }, items: any): Promise<any> {
      let cData: any = formUtils.buildParams(items);
      cData.user_id = accessor.user.userID;
      console.log('__STORE__ cData: ', cData);
      try {
        console.log('__STORE__ create/UpdateCandidate');
        if (accessor.candidate.candidateID) {
          const data = await candidateService.updateCandidate(
            accessor.candidate.candidateID,
            cData,
          );
          console.log('>>>> response update data:', data);
          return data;
        } else {
          const data = await candidateService.createCandidate(cData);
          commit('SET_CANDIDATE_ID', data.id);
          console.log('>>>> response data:', data);
          return data;
        }
      } catch (e) {
        console.log('candidate> response error:', e);
        throw e;
      }
    },

    async getCandidate({ commit }): Promise<any> {
      try {
        console.log('__STORE__ getCandidate');
        let data = await candidateService.getCandidate(accessor.user.userID);
        commit('SET_CANDIDATE_ID', data.id);
        console.log('>>>> response data:', data);
        return data;
      } catch (e) {
        throw e;
      }
    },
  },
);
