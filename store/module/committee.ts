import { committeeService } from '~/api/services/committee.service';
import { accessor } from '~/store';
import { actionTree, mutationTree } from 'typed-vuex/lib';
import { formUtils } from '~/utils/common/forms';

export interface CommitteeState {
  committeeID: string;
}

export const namespaced = true;
export const state = (): CommitteeState => ({
  committeeID: '',
});

export const mutations = mutationTree(state, {
  SET_COMMITTEE_ID(state: CommitteeState, committeeID: string) {
    state.committeeID = committeeID;
  },
});

export const actions = actionTree(
  { state, mutations },
  {
    async cleanData({ commit }) {
      commit('SET_COMMITTEE_ID', '');
    },

    async createCommittee({ commit }, items: any): Promise<any> {
      await accessor.restored;

      console.log('__STORE_BEFORE_ createCommittee: ');
      let cData: any = formUtils.buildParams(items);
      cData.user_id = accessor.user.userID;

      console.log('__STORE_BEFORE_ cData: ', cData);
      try {
        if (accessor.committee.committeeID) {
          const data = await committeeService.updateCommittee(
            accessor.committee.committeeID,
            cData,
          );
          console.log('>>>> response update data:', data);
          return data;
        } else {
          let data = await committeeService.createCommittee(cData);
          commit('SET_COMMITTEE_ID', data.id);
          console.log('__STORE__ createCommittee success: ', data);
          return data;
        }
      } catch (e) {
        console.log('__STORE__ createCommittee error: ', e);
        throw e;
      }
    },

    async getCommittee({ commit }): Promise<any> {
      try {
        console.log('__STORE__ getCommittee');
        let data = await committeeService.getCommittee(accessor.user.userID);
        commit('SET_COMMITTEE_ID', data.id);
        console.log('>>>> response data:', data);
        return data;
      } catch (e) {
        throw e;
      }
    },
  },
);
