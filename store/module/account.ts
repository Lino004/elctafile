import { accessor } from '~/store';
import { actionTree, mutationTree } from 'typed-vuex/lib';
import { accountService } from '~/api/services/account.service';
import { formUtils } from '~/utils/common/forms';

export interface AccountState {
  accountID: string;
}

export const namespaced = true;
export const state = (): AccountState => ({
  accountID: '',
});

export const mutations = mutationTree(state, {
  SET_ACCOUNT_ID(state: AccountState, accountID: string) {
    state.accountID = accountID;
  },
});

export const actions = actionTree(
  { state, mutations },
  {
    async cleanData({ commit }) {
      commit('SET_ACCOUNT_ID', '');
    },

    async createAccount({ commit }, items: any): Promise<any> {
      try {
        await accessor.restored;
        let aData: any = formUtils.buildParams(items);

        aData.user_id = accessor.user.userID;
        if (accessor.account.accountID) {
          const data = await accountService.updateAccount(
            accessor.account.accountID,
            aData,
          );
          console.log('>>>> response update data:', data);
          return data;
        } else {
          console.log('__STORE__ createAccount: ', aData);
          let data = await accountService.createAccount(aData);
          commit('SET_ACCOUNT_ID', data.id);
          console.log('__STORE__ createAccount response:', data);
          return data;
        }
      } catch (e) {
        throw e;
      }
    },

    async getAccount({ commit }): Promise<any> {
      try {
        await accessor.restored;
        console.log('__STORE__ getAccount');
        let data = await accountService.getAccount(accessor.user.userID);
        commit('SET_ACCOUNT_ID', data.id);
        console.log('>>>> response data:', data);
        return data;
      } catch (e) {
        throw e;
      }
    },
  },
);
