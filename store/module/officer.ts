import { accessor } from '~/store';
import { actionTree, mutationTree } from 'typed-vuex/lib';
import { officerService } from '~/api/services/officer.service';
import { committeeUtils } from '~/utils';
import { candidateService } from '~/api/services/candidate.service';
import { formUtils } from '~/utils/common/forms';

export interface OfficerState {
  officerID: string;
}

export const namespaced = true;
export const state = (): OfficerState => ({
  officerID: '',
});

export const mutations = mutationTree(state, {
  SET_OFFICER_ID(state: OfficerState, accountID: string) {
    state.officerID = accountID;
  },
});

export const actions = actionTree(
  { state, mutations },
  {
    async cleanData({ commit }) {
      commit('SET_OFFICER_ID', '');
    },

    async createOfficer({ commit }, items: any): Promise<any> {
      await accessor.restored;
      let aData: any = formUtils.buildParams(items);

      try {
        if (accessor.officer.officerID) {
          const data = await officerService.updateOfficer(
            accessor.officer.officerID,
            aData,
          );
          console.log('>>>> response update data:', data);
          return data;
        } else {
          aData.user_id = accessor.user.userID;
          console.log('__STORE__ createOfficer: ', aData);
          let data = await officerService.createOfficer(aData);
          commit('SET_OFFICER_ID', data.id);

          console.log('__STORE__ createOfficer response', data);
          return data;
        }
      } catch (e) {
        throw e;
      }
    },
    async getOfficer({ commit }): Promise<any> {
      try {
        console.log('__STORE__ getCandidate');
        let data = await officerService.getOfficer(accessor.user.userID);
        commit('SET_OFFICER_ID', data.id);
        console.log('>>>> response data:', data);
        return data;
      } catch (e) {
        throw e;
      }
    },
  },
);
