export const mutations = {
  // @ts-ignore
  showMessage(state, payload) {
    state.message = payload.message;
    state.color = payload.color;
  },
};

export interface SnackbarState {
  message: string;
  error: boolean;
}

export const namespaced = true;
export const state = (): SnackbarState => ({
  message: '',
  error: false,
});

//
// export const actions = actionTree(
//   { state, mutations },
//   {
//     showActionMessage({commit}) {
//       commit('showMessage', { message: '', error: false});
//     },
//   }
// )
