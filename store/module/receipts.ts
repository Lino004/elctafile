import { actionTree, mutationTree } from 'typed-vuex/lib';
import { receiptService } from '~/api/services/receipt.service';
import { accessor } from '~/store';

export interface ReceiptState {
  receipt: any;
}

export const namespaced = true;
export const state = (): ReceiptState => ({
  receipt: {},
});

export const mutations = mutationTree(state, {
  SET_RECEIPT(state: ReceiptState, data: any) {
    state.receipt = data;
  },
});

export const actions = actionTree(
  { state, mutations },
  {
    cleanData() {},

    async getReceipts(): Promise<any> {
      // eslint-disable-next-line no-useless-catch
      try {
        const data = await receiptService.getReceipts(accessor.user.userID);
        return data;
      } catch (e) {
        throw e;
      }
    },
    async getReceipt(context, id): Promise<any> {
      // eslint-disable-next-line no-useless-catch
      try {
        const data = await receiptService.getReceipt(id);
        return data;
      } catch (e) {
        throw e;
      }
    },
    async saveReceipt(context, payload: any): Promise<any> {
      // eslint-disable-next-line no-useless-catch
      try {
        payload.user_id = accessor.user.userID;
        const data = await receiptService.createReceipt(payload);
        return data;
      } catch (e) {
        throw e;
      }
    },
    async updateReceipt(context, payload: any): Promise<any> {
      // eslint-disable-next-line no-useless-catch
      try {
        payload.data.user_id = accessor.user.userID;
        const data = await receiptService.updateReceipt(payload);
        return data;
      } catch (e) {
        throw e;
      }
    },
    async deleteReceipt(context, id): Promise<any> {
      // eslint-disable-next-line no-useless-catch
      try {
        const data = await receiptService.deleteReceipt(id);
        return data;
      } catch (e) {
        throw e;
      }
    },
    addReceiptToStore({ commit }, receipt): void {
      commit('SET_RECEIPT', receipt);
    },
  },
);
