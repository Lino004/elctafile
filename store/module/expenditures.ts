import { actionTree, mutationTree } from 'typed-vuex/lib';
import { expenditureService } from '~/api/services/expenditure.service';
import { accessor } from '~/store';
import { formUtils } from '~/utils/common/forms';
import { TMP } from '~/constants/const';

export interface ExpenditureState {
  expenditureID: string;
}

export const namespaced = true;
export const state = (): ExpenditureState => ({
  expenditureID: '',
});

export const mutations = mutationTree(state, {
  SET_EXPENDITURE_ID(state: ExpenditureState, expenditureID: string) {
    state.expenditureID = expenditureID;
  },
});

export const actions = actionTree(
  { state, mutations },
  {
    cleanData({ commit }) {
      commit('SET_EXPENDITURE_ID', '');
    },

    async createExpenditure(
      { commit },
      iData: {
        items: any;
        expenditureType: string;
        expenditureID?: string;
      },
    ): Promise<any> {
      const cData: any = formUtils.buildExpenditure(iData.items); // Here we need to create new build params for this Expenditure

      cData.user_id = accessor.user.userID;
      cData.expenditure_type = iData.expenditureType;
      // TODO: get from store
      cData.cycle_id = TMP.cycle_id;
      console.log('__STORE__ cData: ', cData);
      try {
        console.log('__STORE__ create/expenditure');
        const data = await expenditureService.createExpenditure(cData);
        commit('SET_EXPENDITURE_ID', data.id);
        console.log('>>>> response data:', data);
        return data;
      } catch (e) {
        console.log('candidate> response error:', e);
        throw e;
      }
    },

    async getExpenditure({ commit }): Promise<any> {
      // eslint-disable-next-line no-useless-catch
      try {
        console.log('__STORE__ getExpenditure');

        const data = await expenditureService.getExpenditures(
          accessor.user.userID,
        );
        commit('SET_EXPENDITURE_ID', data.id);
        console.log('>>>> response data:', data);
        return data;
      } catch (e) {
        throw e;
      }
    },
  },
);
