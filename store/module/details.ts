import { accessor } from '~/store';
import { actionTree, mutationTree } from 'typed-vuex/lib';
import { detailsService } from '~/api/services/details.service';
import { formUtils } from '~/utils/common/forms';

export interface DetailsState {
  detailsID: string;
}

export const namespaced = true;
export const state = (): DetailsState => ({
  detailsID: '',
});

export const mutations = mutationTree(state, {
  SET_DETAILS_ID(state: DetailsState, partyID: string) {
    state.detailsID = partyID;
  },
});

export const actions = actionTree(
  { state, mutations },
  {
    async cleanData({ commit }) {
      commit('SET_DETAILS_ID', '');
    },

    async createDetails({ commit }, items: any): Promise<any> {
      let cData: any = formUtils.buildParams(items);
      cData.user_id = accessor.user.userID;
      console.log('__STORE__ cData: ', cData);
      try {
        console.log('__STORE__ create/update details');
        if (accessor.details.detailsID) {
          const data = await detailsService.updateDetails(
            accessor.details.detailsID,
            cData,
          );
          console.log('>>>> response update data:', data);
          return data;
        } else {
          const data = await detailsService.createDetails(cData);
          commit('SET_DETAILS_ID', data.id);
          console.log('>>>> response data:', data);
          return data;
        }
      } catch (e) {
        console.log('candidate> response error:', e);
        throw e;
      }
    },

    async getDetails({ commit }): Promise<any> {
      try {
        console.log('__STORE__ getDetails');
        let data = await detailsService.getDetails(accessor.user.userID);
        commit('SET_DETAILS_ID', data.id);
        console.log('>>>> response data:', data);
        return data;
      } catch (e) {
        throw e;
      }
    },
  },
);
