import { actionTree, mutationTree } from 'typed-vuex/lib';
import { counterpartyService } from '~/api/services/counterparty.service';
import { accessor } from '~/store';
import { CounterpartyKeys } from '~/api/electafile-api/types/counterparty';
import { formUtils } from '~/utils/common/forms';
import { TMP } from '~/constants/const';

export interface CounterpartyState {}

export const namespaced = true;
export const state = (): CounterpartyState => ({});

export const mutations = mutationTree(state, {});

export const actions = actionTree(
  { state, mutations },
  {
    async cleanData({ commit }) {},

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async createCounterparty(
      { commit },
      iData: {
        items: any;
        counterpartyType: string;
        counterpartyID?: string;
      },
    ): Promise<any> {
      const cData: any = formUtils.buildParams(iData.items);
      cData.user_id = accessor.user.userID;
      cData[CounterpartyKeys.COUNTERPARTY_TYPE] = iData.counterpartyType;
      // TODO: get from store
      cData.cycle_id = TMP.cycle_id;

      console.log('__STORE__ cData: ', cData);
      try {
        if (iData.counterpartyID) {
          const data = await counterpartyService.updateCounterparty(
            iData.counterpartyID,
            cData,
          );
          console.log('>>>> response update data:', data);
          return data;
        } else {
          const data = await counterpartyService.createCounterparty(cData);
          console.log('>>>> response data:', data);
          return data;
        }
      } catch (e) {
        console.log('counterparty response error:', e);
        throw e;
      }
    },

    async getCounterparties({ commit }): Promise<any> {
      // eslint-disable-next-line no-useless-catch
      try {
        console.log('__STORE__ getCounterparties');
        const data = await counterpartyService.getCounterparties(
          accessor.user.userID,
        );

        console.log('__STORE__ response data:', data);
        return data;
      } catch (e) {
        throw e;
      }
    },

    async getCounterparty({ commit }, counterpartyID: string): Promise<any> {
      try {
        console.log('__STORE__ getCounterparty');
        const data = await counterpartyService.getCounterparty(counterpartyID);

        console.log('>>>> response data:', data);
        return data;
      } catch (e) {
        throw e;
      }
    },
  },
);
