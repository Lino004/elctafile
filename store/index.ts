// TODO: export all from module

// export * from './module/user'
// export * from './module/api'
// export * from './module/authZero'

import Vue from 'vue';
import Vuex, { Store } from 'vuex';
import { useAccessor, getterTree } from 'typed-vuex/lib';
import VuexPersist from 'vuex-persist';
import * as user from './module/user';
import * as committee from './module/committee';
import * as candidate from './module/candidate';
import * as account from './module/account';
import * as officer from './module/officer';
import * as party from './module/party';
import * as details from './module/details';
import * as expenditures from './module/expenditures';
import * as counterparties from './module/counterparties';
import * as snackbar from './module/snackbar';
import * as receipts from './module/receipts';

interface RootState {
  restored: Promise<RootState> | undefined;
}
const state = (): RootState => ({
  restored: undefined,
});

const getters = getterTree(state, {
  restored: (state) => state.restored,
});

const vuexLocal = new VuexPersist({
  key: 'electafile.state',
  storage: window.localStorage,
  reducer: (state: any) => {
    return {
      user: {
        userID: state.user.userID,
        authID: state.user.authID,
        email: state.user.email,
        name: state.user.name,
        role: state.user.role,
        committee_type: state.user.committee_type,
        is_committee_completed: state.user.is_committee_completed,
        is_logged_in: state.user.is_logged_in,
      },
      committee: {
        committeeID: state.committee.committeeID,
      },
      candidate: {
        candidateID: state.candidate.candidateID,
      },
      account: {
        accountID: state.account.accountID,
      },
      officer: {
        officerID: state.officer.officerID,
      },
      party: {
        partyID: state.party.partyID,
      },
      details: {
        detailsID: state.details.detailsID,
      },
      expenditures: {},
      counterparties: {},
      snackbar: {},
    };
  },
});

Vue.use(Vuex);

const storePattern = {
  state,
  getters,
  modules: {
    // sidebar,
    // sitedevice,
    user,
    committee,
    candidate,
    account,
    officer,
    party,
    details,
    expenditures,
    counterparties,
    snackbar,
    receipts,
  },
  plugins: [vuexLocal.plugin],
};

export const store = new Store(storePattern);

export const accessor = useAccessor(store, storePattern);

// Optionally, inject accessor globally
Vue.prototype.$accessor = accessor;

// export default store;
