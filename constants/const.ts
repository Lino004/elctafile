import { CommitteeType } from '~/api/electafile-api/types/user';

export const VIEW_MODE = {
  COMMITTEE: 'committee',
  SETTINGS: 'settings',
  REPORTS: 'reports',
  NONE: 'none',
};

export const COMMITTE_ORGANIZATION_LEVEL = [
  { label: 'State', value: 'state' },
  { label: 'County', value: 'county' },
];

export const PARTY_TYPES = [
  { label: 'Executive', value: 'executive' },
  { label: 'Affiliated', value: 'affiliated' },
  { label: 'Subordinate', value: 'subordinate' },
];

// TODO: refactor key = value
export const ACCOUNT_TYPES = [
  { label: 'CD', value: 'cd' },
  { label: 'Checking', value: 'checking' },
  { label: 'Credit Card', value: 'credit_card' },
  { label: 'Money Market', value: 'money_market' },
  { label: 'Savings', value: 'savings' },
];

export const COMMITTEE_TYPE_OPTIONS = [
  { label: 'Candidate Committee', value: CommitteeType.CANDIDATE_COMMITTEE },
  { label: 'Party Committee', value: CommitteeType.PARTY_COMMITTEE },
  {
    label: 'Political Action Committee',
    value: CommitteeType.POLITICAL_ACTION_COMMITTEE,
  },
  {
    label: 'Independent Expenditure Committee',
    value: CommitteeType.INDEPENDENT_EXPENDITURE_COMMITTEE,
  },
];

// TODO: refactor key = value
export const PAC_CATEGORY = [
  { label: 'Banking / Finance', value: 'banking' },
  { label: 'Building / Real Estate', value: 'building' },
  { label: 'Conservative / Liberal', value: 'conservative' },
  { label: 'Environment', value: 'environment' },
  { label: 'Get Out the Vote', value: 'out_of_vote' },
  { label: 'Health', value: 'health' },
  {
    label: 'Information Technology / Telecommunications',
    value: 'information_technology',
  },
  { label: 'Insurance', value: 'insurance' },
  { label: 'Legal', value: 'legal' },
  { label: 'Manufacturing', value: 'manufacturing' },
  { label: 'Minority', value: 'minority' },
  {
    label: 'Political Party not part of Party Plan of Org.',
    value: 'political_party_not_part_of_party_plan_of',
  },
  { label: 'Religious', value: 'religious' },
  { label: 'Trade', value: 'trade' },
  { label: 'Utilities', value: 'utilities' },
  { label: 'Other / Not listed', value: 'other' },
];

export const PAC_TYPE = [
  { label: 'Parent Entity', value: 'parent_entity' },
  { label: 'Economic Interest', value: 'economic_interest' },
  { label: 'Political Purpose', value: 'political_purpose' },
];

// TODO: refactor map values
export const PURPOSE_CODE = [
  { label: 'A - Media', value: 'a' },
  { label: 'B - Printing', value: 'b' },
  { label: 'C - Fundraising', value: 'c' },
  { label: 'D - To Another Candidate', value: 'd' },
  { label: 'E - Salaries', value: 'e' },
  { label: 'F - Equipment', value: 'f' },
  { label: 'G - Political Party', value: 'g' },
  {
    label: 'H - Holding Public Office Expense',
    value: 'h',
  },
  { label: 'I - Postage', value: 'i' },
  { label: 'J - Penalties', value: 'j' },
  { label: 'K - Office Expenses', value: 'k' },
  {
    label: 'Q - Donation to Legal Expse Fund',
    value: 'q',
  },
  { label: 'O - Other', value: 'or' },
];

// TODO: refactor map values
export const CONDITIONAL_PURPOSE = ['a', 'b', 'c', 'f', 'h', 'k', 'o'];

// TODO: refactor map values
// TODO: remove, will retrieve from counterparties
export const PAYEE = [
  { label: 'Ronald Richards', value: 'ronald-richards' },
  { label: 'Dianne Russell', value: 'dianne-russell' },
  { label: 'Esther Howard', value: 'esther-howard' },
  { label: 'Company, LLC', value: 'company-llc' },
  { label: 'Franklyn’s Fund', value: 'franklyn’s-fund' },
  { label: 'Sarah Wells', value: 'sarah-wells' },
  { label: 'Wade Warren', value: 'wade-warren' },
];

// TODO: refactor map values
export const EXPENDITURE_LIST = [
  {
    committeeTypes: ['candidate', 'independent', 'pac'],
  },
  {
    committeeTypes: ['party'],
  },
];

export const CONTRIBUTOR = [
  { label: 'Ronald Richards', value: 'ronald-richards' },
  { label: 'Dianne Russell', value: 'dianne-russell' },
  { label: 'Esther Howard', value: 'esther-howard' },
  { label: 'Company, LLC', value: 'company-llc' },
  { label: 'Franklyn’s Fund', value: 'franklyn’s-fund' },
  { label: 'Sarah Wells', value: 'sarah-wells' },
  { label: 'Wade Warren', value: 'wade-warren' },
];

export const EXPENDITURES = [
  { label: '$100 to Wade Warren (01-31-2022)', value: 'wade-warren' },
  { label: '$5,520.00 to Company Abcz (01-15-2022)', value: 'company-abcz' },
];

export const LOAN_FORGIVEN = [
  {
    label: '$1,000.00 by Franklyn’s Fund (01-31-2022 - 06-29-2022)',
    value: 'franklyn’s-fund',
  },
  {
    label: '$91,520.00 by Company Abcz (01-15-2022 - 11-11-2022)',
    value: 'company-abcz',
  },
];

export const PLAN = [
  {
    label: 'September 2022 Apple Pie Sale',
    value: 0,
    date: '2021-10-06',
  },
];

// export const ALL_EXPENDITURES_TYPE = [
//   'Operating Expense',
//   'Contribution to Candidate or Political Committee',
//   'Coordinated Party Expenditure',
// ];

export const OUTSTANDING_LOAN = [
  {
    label: '$1,000.00 by Franklyn’s Fund (01-31-2022 - 06-29-2022)',
    value: 'franklyn’s-fund',
  },
  {
    label: '$91,520.00 by Company Abcz (01-15-2022 - 11-11-2022)',
    value: 'company-abcz',
  },
];
export const TYPE_GIFT = [
  {
    label: 'Coordinated Party Expenditure',
    value: 'coordinated_party_expenditure',
  },
  {
    label: 'Contribution to Candidate/Political Committee',
    value: 'contribution_to_candidate/political_committee',
  },
];
export const REFUND_PURPOSE_CODE = [
  { label: 'L - Returned to Contributor', value: 'l' },
  { label: 'M - Overpayment for Service', value: 'm' },
  { label: 'N - Exceeded Contribution Limit', value: 'n' },
  { label: 'P - Reimbursement of In-Kind', value: 'p' },
  { label: 'O - Other', value: 'o' },
];
// This is just a checker for REFUND_PURPOSE_CODE conditionally
export const REFUND_CONDITIONAL_PURPOSE = ['p', 'o'];

export const PARTY_AFFILIATION = [
  { label: 'Democratic', value: 'democratic' },
  { label: 'Republican', value: 'republican' },
  { label: 'Natural Law', value: 'natural_law' },
  { label: 'Libertarian', value: 'libertarian' },
  { label: 'Reform', value: 'reform' },
  { label: 'Unaffiliated', value: 'unaffiliated' },
  { label: 'Southern', value: 'southern' },
  { label: 'Green', value: 'green' },
  { label: 'Non-Partisan', value: 'non_partisan' },
  { label: 'Other', value: 'other' },
];

// TODO: we have main committee type, we shouldn't use the same naming here
export const COMMITTEE_TYPE = [
  { label: 'Political Party Committee', value: 'political-party-committee' },
  { label: 'Political Action Committee', value: 'political-action-committee' },
  { label: 'Legal Expense Fund', value: 'legal-expense-fund' },
  { label: 'Joint Candidate Committee', value: 'joint-candidate-committee' },
  { label: 'Referendum Committee', value: 'referendum-committee' },
  {
    label: 'Independent Expenditure PAC',
    value: 'independent-expenditure-pac',
  },
];
export const IS_UI_ONLY = false;
export const IS_DEBUG = false;

export const FEATURE_FLAGS = {
  AUTH: true,
  COMMITTEE: true,
  SETTINGS: true,
  COUNTERPARTIES: true,
  EXPENDITURES: false,
  RECEIPTS: false,
  REPORTS: false,
  CYCLES: false,
};

export const TMP = {
  cycle_id: '1cdad10a-068c-4764-94d1-6e3700ab901f',
};
