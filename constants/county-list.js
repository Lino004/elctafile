export default [
  {
    label: 'Select a County',
    value: '',
  },
  {
    label: 'ALAMANCE',
    value: '1',
  },
  {
    label: 'ALEXANDER',
    value: '2',
  },
  {
    label: 'ALLEGHANY',
    value: '3',
  },
  {
    label: 'ANSON',
    value: '4',
  },
  {
    label: 'ASHE',
    value: '5',
  },
  {
    label: 'AVERY',
    value: '6',
  },
  {
    label: 'BEAUFORT',
    value: '7',
  },
  {
    label: 'BERTIE',
    value: '8',
  },
  {
    label: 'BLADEN',
    value: '9',
  },
  {
    label: 'BRUNSWICK',
    value: '10',
  },
  {
    label: 'BUNCOMBE',
    value: '11',
  },
  {
    label: 'BURKE',
    value: '12',
  },
  {
    label: 'CABARRUS',
    value: '13',
  },
  {
    label: 'CALDWELL',
    value: '14',
  },
  {
    label: 'CAMDEN',
    value: '15',
  },
  {
    label: 'CARTERET',
    value: '16',
  },
  {
    label: 'CASWELL',
    value: '17',
  },
  {
    label: 'CATAWBA',
    value: '18',
  },
  {
    label: 'CHATHAM',
    value: '19',
  },
  {
    label: 'CHEROKEE',
    value: '20',
  },
  {
    label: 'CHOWAN',
    value: '21',
  },
  {
    label: 'CLAY',
    value: '22',
  },
  {
    label: 'CLEVELAND',
    value: '23',
  },
  {
    label: 'COLUMBUS',
    value: '24',
  },
  {
    label: 'CRAVEN',
    value: '25',
  },
  {
    label: 'CUMBERLAND',
    value: '26',
  },
  {
    label: 'CURRITUCK',
    value: '27',
  },
  {
    label: 'DARE',
    value: '28',
  },
  {
    label: 'DAVIDSON',
    value: '29',
  },
  {
    label: 'DAVIE',
    value: '30',
  },
  {
    label: 'DUPLIN',
    value: '31',
  },
  {
    label: 'DURHAM',
    value: '32',
  },
  {
    label: 'EDGECOMBE',
    value: '33',
  },
  {
    label: 'FORSYTH',
    value: '34',
  },
  {
    label: 'FRANKLIN',
    value: '35',
  },
  {
    label: 'GASTON',
    value: '36',
  },
  {
    label: 'GATES',
    value: '37',
  },
  {
    label: 'GRAHAM',
    value: '38',
  },
  {
    label: 'GRANVILLE',
    value: '39',
  },
  {
    label: 'GREENE',
    value: '40',
  },
  {
    label: 'GUILFORD',
    value: '41',
  },
  {
    label: 'HALIFAX',
    value: '42',
  },
  {
    label: 'HARNETT',
    value: '43',
  },
  {
    label: 'HAYWOOD',
    value: '44',
  },
  {
    label: 'HENDERSON',
    value: '45',
  },
  {
    label: 'HERTFORD',
    value: '46',
  },
  {
    label: 'HOKE',
    value: '47',
  },
  {
    label: 'HYDE',
    value: '48',
  },
  {
    label: 'IREDELL',
    value: '49',
  },
  {
    label: 'JACKSON',
    value: '50',
  },
  {
    label: 'JOHNSTON',
    value: '51',
  },
  {
    label: 'JONES',
    value: '52',
  },
  {
    label: 'LEE',
    value: '53',
  },
  {
    label: 'LENOIR',
    value: '54',
  },
  {
    label: 'LINCOLN',
    value: '55',
  },
  {
    label: 'MACON',
    value: '56',
  },
  {
    label: 'MADISON',
    value: '57',
  },
  {
    label: 'MARTIN',
    value: '58',
  },
  {
    label: 'MCDOWELL',
    value: '59',
  },
  {
    label: 'MECKLENBURG',
    value: '60',
  },
  {
    label: 'MITCHELL',
    value: '61',
  },
  {
    label: 'MONTGOMERY',
    value: '62',
  },
  {
    label: 'MOORE',
    value: '63',
  },
  {
    label: 'NASH',
    value: '64',
  },
  {
    label: 'NEW HANOVER',
    value: '65',
  },
  {
    label: 'NORTHAMPTON',
    value: '66',
  },
  {
    label: 'ONSLOW',
    value: '67',
  },
  {
    label: 'ORANGE',
    value: '68',
  },
  {
    label: 'PAMLICO',
    value: '69',
  },
  {
    label: 'PASQUOTANK',
    value: '70',
  },
  {
    label: 'PENDER',
    value: '71',
  },
  {
    label: 'PERQUIMANS',
    value: '72',
  },
  {
    label: 'PERSON',
    value: '73',
  },
  {
    label: 'PITT',
    value: '74',
  },
  {
    label: 'POLK',
    value: '75',
  },
  {
    label: 'RANDOLPH',
    value: '76',
  },
  {
    label: 'RICHMOND',
    value: '77',
  },
  {
    label: 'ROBESON',
    value: '78',
  },
  {
    label: 'ROCKINGHAM',
    value: '79',
  },
  {
    label: 'ROWAN',
    value: '80',
  },
  {
    label: 'RUTHERFORD',
    value: '81',
  },
  {
    label: 'SAMPSON',
    value: '82',
  },
  {
    label: 'SCOTLAND',
    value: '83',
  },
  {
    label: 'STANLY',
    value: '84',
  },
  {
    label: 'STOKES',
    value: '85',
  },
  {
    label: 'SURRY',
    value: '86',
  },
  {
    label: 'SWAIN',
    value: '87',
  },
  {
    label: 'TRANSYLVANIA',
    value: '88',
  },
  {
    label: 'TYRRELL',
    value: '89',
  },
  {
    label: 'UNION',
    value: '90',
  },
  {
    label: 'VANCE',
    value: '91',
  },
  {
    label: 'WAKE',
    value: '92',
  },
  {
    label: 'WARREN',
    value: '93',
  },
  {
    label: 'WASHINGTON',
    value: '94',
  },
  {
    label: 'WATAUGA',
    value: '95',
  },
  {
    label: 'WAYNE',
    value: '96',
  },
  {
    label: 'WILKES',
    value: '97',
  },
  {
    label: 'WILSON',
    value: '98',
  },
  {
    label: 'YADKIN',
    value: '99',
  },
  {
    label: 'YANCEY',
    value: '100',
  },
];
