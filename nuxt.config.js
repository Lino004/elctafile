import main from './assets/themes/ef-main';

export default {
  ssr: false,
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - electafilenuxt',
    title: 'electafilenuxt',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['@/assets/reset.css', '@/assets/fonts.css'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ['~/plugins/global.js', '~/plugins/notifier.js'],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    '@nuxtjs/dotenv',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: ['@nuxtjs/axios', '@nuxtjs/auth-next'],

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      themes: {
        light: main,
      },
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},

  // Storyboook
  storybook: {
    // Options
  },

  publicRuntimeConfig: {
    NUXT_ENV_AUTHENTICATION_AUTH0_DOMAIN:
      process.env.NUXT_ENV_AUTHENTICATION_AUTH0_DOMAIN,
    NUXT_ENV_AUTHENTICATION_AUTH0_CLIENT_ID:
      process.env.NUXT_ENV_AUTHENTICATION_AUTH0_CLIENT_ID,
    NUXT_ENV_AUTHENTICATION_AUTH0_AUDIENCE:
      process.env.NUXT_ENV_AUTHENTICATION_AUTH0_AUDIENCE,
    NUXT_ENV_API_BASE_URL: process.env.NUXT_ENV_API_BASE_URL,
  },

  auth: {
    redirect: {
      callback: '/callback',
      logout: '/',
    },
    strategies: {
      auth0: {
        domain: 'electafile-slm.us.auth0.com', // process.env.NUXT_ENV_AUTHENTICATION_AUTH0_DOMAIN, // 'electafile-slm.us.auth0.com',
        clientId: 'S5Qzzop9BVYzmpE3nfO9va50anjtkWeP', // process.env.NUXT_ENV_AUTHENTICATION_AUTH0_CLIENT_ID, // 'S5Qzzop9BVYzmpE3nfO9va50anjtkWeP',
        audience: 'https://electafile-slm.us.auth0.com/api/v2/', // process.env.NUXT_ENV_AUTHENTICATION_AUTH0_AUDIENCE, // 'https://electafile-slm.us.auth0.com/api/v2/',
      },
    },
  },
};
// responseType: 'token id_token',
// audience: 'https://dev--7vlzx8k.us.auth0.com/api/v2/',
//   scope: ['openid', 'profile', 'email'],
//   responseType: 'code',
//   redirect_uri: 'http://localhost:3000/callback',
//   logoutRedirectUri: 'http://localhost:3000/callback',

//   audience: 'https://electafile-slm.us.auth0.com/api/v2/',
// redirect_uri: 'http://localhost:3000',
