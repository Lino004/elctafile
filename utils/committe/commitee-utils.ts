export const committeeUtils = {
  getPathName: function getPathName(commiteeType: string) {
    return commiteeType + '-committee';
  },
};
