import {
  CounterpartyKeys,
  CounterpartyType,
} from '~/api/electafile-api/types/counterparty';
import { utils } from '~/utils/common/utils';

export const counterpartyUtils = {
  buildTableItems: function buildTableItems(items: any) {
    const counterpartiesItems: any[] = [];
    items.forEach((item: any) => {
      const counterpartyType = item[CounterpartyKeys.COUNTERPARTY_TYPE];

      // form name data
      let name = item[CounterpartyKeys.FULL_NAME];
      switch (counterpartyType) {
        case CounterpartyType[0].value: // individual
          name = item[CounterpartyKeys.FULL_NAME];
          break;

        default:
          name = item[CounterpartyKeys.ORGANIZATION_NAME];
          break;
      }

      const _item = {
        id: item.id,
        name,
        counterpartyType: utils.findItemByValue(
          CounterpartyType,
          counterpartyType,
        ).label,
        // TODO: clarify about calculations
        contributedTotal: '$1,542.00',
        gotPaidTotal: '$490.00',
      };
      counterpartiesItems.push(_item);
    });
    return counterpartiesItems;
  },
};
