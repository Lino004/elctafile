import { accessor } from '~/store';

export const userUtils = {
  logout: async function logout() {
    await accessor.restored;

    await accessor.user.cleanData();
    await accessor.account.cleanData();
    await accessor.candidate.cleanData();
    await accessor.committee.cleanData();
    await accessor.details.cleanData();
    await accessor.officer.cleanData();
    await accessor.party.cleanData();
  },
};
