export const utils = {
  findItemByValue: function findItemByValue(items: any, itemValue: string) {
    const item = items.find((item: any) => {
      return item.value === itemValue;
    });
    return item;
  },
  findItemByKey: function findItemByKey(items: any, itemKey: string) {
    const item = items.find((item: any) => {
      return item.key === itemKey;
    });
    return item;
  },
  capitalizeFirstLetter: function capitalizeFirstLetter(str: string) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  },
  random: function getRandomInt(max: number) {
    return Math.floor(Math.random() * max);
  },
};
