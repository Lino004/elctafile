import {
  ACCOUNT_TYPES,
  COMMITTE_ORGANIZATION_LEVEL,
  PAC_CATEGORY,
  PAC_TYPE,
  PARTY_AFFILIATION,
  PARTY_TYPES,
} from '~/constants/const';
import countyList from '~/constants/county-list';
import { AddressKeys } from '~/api/electafile-api/types/address';
import { dateUtils } from '~/utils/common/date';

export const formUtils = {
  buildSelector: function buildSelector(key: string, value: string) {
    let selected = null;
    switch (key) {
      case 'committee_organization_level':
        selected = COMMITTE_ORGANIZATION_LEVEL.find((el) => {
          return el.value === value;
        });
        return selected;

      case 'party_affiliation_type':
        selected = PARTY_AFFILIATION.find((el) => {
          return el.value === value;
        });

        return selected;

      case 'account_type':
        selected = ACCOUNT_TYPES.find((el) => {
          return el.value === value;
        });
        console.log('account_type:', selected);
        return selected;

      case 'party_type':
        selected = PARTY_TYPES.find((el) => {
          return el.value === value;
        });
        console.log('party_type:', selected);
        return selected;

      case 'pac_type':
        selected = PAC_TYPE.find((el) => {
          return el.value === value;
        });
        console.log('pac_type:', selected);
        return selected;

      case 'pac_category':
        selected = PAC_CATEGORY.find((el) => {
          return el.value === value;
        });
        console.log('pac_type:', selected);
        return selected;

      case 'county':
        selected = countyList.find((el) => {
          return el.label === value;
        });
        return selected;

      case 'full_name':
        selected = value;
        return selected;

      default:
        return selected;
    }
  },

  buildExpenditure: function buildExpenditure(items: any) {
    const data: any = {};

    items.forEach((item: any) => {
      if (item.label === 'Amount') {
        data.amount = item.value;
      } else if (item.label === 'Date') {
        data.entry_date = item.value;
      } else if (item.label === 'Payee') {
        data.political_committee_counterparty_id = 'Company, LLC'; // ?
      } else if (item.label === 'Purpose Code') {
        data.expenditure_purpose_codes = 'Fundraising';
      } else if (item.label === 'Required Remarks') {
        data.remarks = item.value;
      } else if (item.label === 'Comments') {
        data.comments = item.value;
      }
    });

    data.payment_type = 'cash';
    data.payment_info = {};
    data.counterparty_id = '3354a519-e0dd-4a35-916e-a4bc4adc9230';
    console.log(`data to send ${data}`);
    return data;
  },

  buildParams: function buildParams(items: any) {
    const data: any = {};
    items.forEach((item: any) => {
      if (item.key) {
        console.log(`buildParams key: ${item.key}`);
        console.log(`buildParams value: ${item.value}`);
        if (item.parent_key) {
          if (!data[item.parent_key]) {
            data[item.parent_key] = {};
          }

          if (item.key === AddressKeys.COUNTY) {
            data[item.parent_key][item.key] = item.value?.label ?? '';
          } else if (item.key === AddressKeys.CITY) {
            data[item.parent_key][item.key] = item.value ?? '';
          } else if (item.type === 'select') {
            data[item.parent_key][item.key] = item.value?.value ?? '';
          } else {
            data[item.parent_key][item.key] = item.value ?? '';
          }
        } else if (item.variant === 'datepicker') {
          data[item.key] = item.data ?? '';
        } else if (item.type === 'select') {
          data[item.key] = item.value.value ?? '';
        } else if (item.type === 'checkbox') {
          if (item.subtype === 'boolean') {
            data[item.key] = item.value ?? false;
          }
        } else {
          data[item.key] = item.value ?? '';
        }
      }
    });
    console.log(`data to send ${data}`);
    return data;
  },

  buildItems: function buildItems(items: any, data: any) {
    items.forEach((item: any) => {
      if (item.key) {
        if (item.parent_key) {
          if (data[item.parent_key]) {
            if (item.key === AddressKeys.STATE) {
              const itemData = data[item.parent_key][item.key] ?? '';
              const selected = { label: itemData, value: itemData };
              item.selected = selected;
            } else if (item.key === AddressKeys.CITY) {
              item.selected = data[item.parent_key][item.key] ?? '';
            } else if (item.key === AddressKeys.COUNTY) {
              // we adding this item directly in view
              // data[item.parent_key][item.key] = item.value?.label ?? '';
            } else {
              item.value = data[item.parent_key][item.key] ?? '';
            }
          }
        } else if (item.variant === 'datepicker') {
          item.selected = dateUtils.formatDate(data[item.key]) ?? ''; // '2021-10-04'
        } else if (item.type === 'select') {
          const selected = this.buildSelector(item.key, data[item.key]);
          item.selected = selected;
        } else {
          item.value = data[item.key] ?? '';
        }
      }
    });
    return items;
  },

  findItem: function findItem(items: any, itemKey: string) {
    const item = items.find((item: any) => {
      return item.key === itemKey;
    });
    return item;
  },
};
