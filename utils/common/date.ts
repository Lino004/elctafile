export const dateUtils = {
  formatDate: function formatDate(date: string) {
    if (!date) {
      return '';
    }
    console.log(`>>> formatDate date:`, date);
    const parts = date.split('T');
    // @ts-ignore
    if (parts.length === 0) {
      return '';
    }
    const partsDate = parts[0].split('-');
    if (partsDate.length < 3) {
      return '';
    }
    console.log(`>>> formatDate partsDate:`, partsDate);
    return `${partsDate[0]}-${partsDate[1]}-${partsDate[2]}`;
  },
};
