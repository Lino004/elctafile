export default {
  title: 'TabsVertical Component',
};
const steps = [
  { title: 'Tab 1', content: 'Tab 1', demo: true },
  { title: 'Tab 2', content: 'Tab 2', demo: true },
  { title: 'Tab 3', content: 'Tab 3', demo: true },
];

export const defaultTabsComponent = () => ({
  template: `<v-app><EFTabsVertical :steps="steps" /></v-app>`,
  data: () => ({ steps }),
});
