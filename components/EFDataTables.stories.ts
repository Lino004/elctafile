export default { title: 'DataTables' };

export const defaultDataTables = () => '<v-app><EFDataTables  /><v-app>';
export const disabledDataTables = () =>
  '<v-app><EFDataTables disabled value="Disabled" /><v-app>';
