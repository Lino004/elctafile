export default { title: 'Checkbox' };

export const defaultCheckbox = () =>
  '<v-app><EFCheckbox dark="true" color="indigo" type="disabled"  /><v-app>';
export const multipleCheckbox = () =>
  `<v-app><EFCheckbox color="indigo" indeterminate value="Indeterminate"  /><v-app >`;
export const disabledOnCheckbox = () =>
  `<v-app><EFCheckbox input-value="true" color="indigo" disabled  value ="Disabled"/><v-app >`;
export const disabledOffCheckbox = () =>
  `<v-app><EFCheckbox  color="indigo" disabled  value ="Disabled"/><v-app >`;
