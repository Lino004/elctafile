export default { title: 'StatusLabel' };

export const labelDraft = () =>
  '<v-app><EFStatusLabel status="draft" title="Draft"></EFStatusLabel><v-app>';
export const labelReady = () =>
  '<v-app><EFStatusLabel status="ready" title="READY"></EFStatusLabel><v-app>';
export const labelPending = () =>
  '<v-app><EFStatusLabel status="pending" title="PENDING"></EFStatusLabel></v-app>';
export const labelSent = () =>
  '<v-app><EFStatusLabel status="sent" title="SENT"></EFStatusLabel></v-app>';
export const labelAccepted = () =>
  '<v-app><EFStatusLabel status="accepted" title="ACCEPTED"></EFStatusLabel></v-app>';
export const labelRejected = () =>
  '<v-app><EFStatusLabel status="rejected" title="REJECTED"></EFStatusLabel></v-app>';
