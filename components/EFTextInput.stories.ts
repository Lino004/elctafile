export default { title: 'InputText' };

export const defaultInput = () =>
  '<v-app><EFTextInput label="Default Text Input" /><v-app>';
export const disabledInput = () =>
  `<v-app><EFTextInput disabled label="Disabled Text Input" value ="Disabled"/><v-app >`;
export const errorInput = () =>
  `<v-app><EFTextInput isError :rules = "[(value) => false]" label="Error Text Input" error :errorMessages = "['Invalid Text Input']" value ="Invalid"/><v-app >`;
