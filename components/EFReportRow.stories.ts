export default { title: 'ReportRow' };

export const defaultReportRow = () => '<v-app><EFReportRow  /><v-app>';
export const disabledReportRow = () =>
  '<v-app><EFReportRow  disabled  value ="Disabled" /><v-app>';
export const hoverReportRow = () => '<v-app><EFReportRow  /><v-app>';
