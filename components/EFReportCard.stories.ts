import Q1 from '../assets/images/Q1.vue';
export default { title: 'Report Card23' };

const cardContents = {
  image: Q1,
  title: 'First Quarter',
  dueDate: 'February 28',
};

export const defaultReportCard = () => ({
  template: `<v-app><EFReportCard :content="cardContents" /></v-app>`,
  data: () => ({ cardContents }),
});
