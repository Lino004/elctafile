export default {
  title: 'Dashboard Banner',
};

const dashBoardBannerContents = {
  header: 'NC Statewide General Elections',
  dividers: [
    { title: 'Committee', subtitle: 'Ronald Richards Committee' },
    { title: 'Committee ID', subtitle: 'STA-123456-C-001' },
  ],
  inCard: [
    { title: 'Primary', date: 'Mar 8, 2022' },
    { title: 'General', date: 'Nov 8, 2020' },
  ],
  linkText: 'NC Board of Elections',
  linkUrl: 'https://www.lipsum.com/',
};

export const dashboardBanner = () => ({
  template: `<v-app><EFDashboardBanner :content="dashBoardBannerContents" /></v-app>`,
  data: () => ({ dashBoardBannerContents }),
});
