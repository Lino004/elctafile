export default { title: 'Button' };

export const defaultButton = () =>
  '<v-app><EFButton icon ="">Default Button</EFButton><v-app>';
export const primaryButton = () =>
  '<v-app><EFButton icon="" type="primary">Primary Button</EFButton><v-app>';
export const secondaryButton = () =>
  '<v-app><EFButton icon ="" type="secondary">Secondary Button</EFButton></v-app>';
export const withIconButton = () =>
  '<v-app><EFButton icon ="mdi-plus" >Button with Icon</EFButton></v-app>';
export const disabledButton = () =>
  '<v-app><EFButton disbaled="true">Disabled Button</EFButton></v-app>';
