export default { title: 'CardButton' };

export const defaultIconCardButton = () => '<v-app><EFCardButtonIcon /><v-app>';
export const defaultTextCardButton = () =>
  `<v-app><EFCardButtonText  /><v-app >`;
