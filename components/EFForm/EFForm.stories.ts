export const exampleLoginForm = () => ({
  template: `<v-app>
    <v-container>
    <EFForm name="Login" :items="loginItems">
    <div class="text-center">
            <EFButton  type="primary"  > Continue </EFButton>
          </div>
    </EFForm>
    </v-container>
    </v-app>`,
  data: () => ({
    loginItems: [
      {
        id: 'login_email',
        type: 'email',
        required: true,
        label: 'Email',
      },
      {
        id: 'login_password',
        type: 'password',
        label: 'Password',
        disabled: true,
      },
    ],
  }),
  mounted() {
    // @ts-ignore
    this.$nuxt.$on('getItem', (res) => {
      console.log(res);
    });
    // @ts-ignore
    this.$nuxt.$on('Login', (res: any) => {
      console.log(res);
    });
  },
});

// export const

export default { title: 'EFForm' };
