export default { title: 'Select Component' };

export const defaultSelectComponent = () =>
  `<v-app> <EFSelectComponent :items="['item1','item2','item3']" label="Default Select List" ></EFSelectComponent><v-app>`;
export const disabledSelectComponent = () =>
  `<v-app> <EFSelectComponent disabled :items="['item1','item2','item3']" label="Disabled Select List" ></EFSelectComponent><v-app>`;
export const datePickerSelectComponent = () =>
  `<v-app><EFSelectComponent  type="datepicker" label="Default DatePicker" ></EFSelectComponent><v-app>`;
