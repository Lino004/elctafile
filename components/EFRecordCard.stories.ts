export default { title: 'Record Card' };

const records = {
  image: require('../assets/images/add-receipt.png'),
  title: 'Add Receipt',
  subtitle: 'General Contribution, Loan Proceed, Interest',
};

export const recordCard = () => ({
  template: `<v-app><EFRecordCard :content="records" /></v-app>`,
  data: () => ({ records }),
});
