export default { title: 'Modal component' };

const modalContent = {
  title: 'Add Transactions. Then, File Reports',
  image: require('../assets/images/file-reports.svg'),
  subText:
    'Add all the transactions from the committee’s bank account(s).When the time comes, Electafile suggests a report that needs to be filed',
  hightlightTitle: 'Practical Tip',
  highlightSubtext:
    'Add transactions as they come. This way, we can notify you as soon as possible if anything doesn’t comply with the law or requires any actions from your side.',
};

export const defaultModal = () => ({
  template: `<v-app><EFModal style="width: 661px;" :highlightText="true" :content="modalContent"><EFButton>Continue</EFButton></EFModal></v-app>`,
  data: () => ({ modalContent }),
});
