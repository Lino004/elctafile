// Global javascript file
const global = {
  dateCounter(date) {
    let val = null;
    const difference = new Date() - date;
    const day = Math.floor(difference / 60000 / 60 / 24);
    // val = day === '1' ? day + ' day ago' : day + ' days ago'
    val = day;
    return val;
  },
  shortForm(x) {
    let shortForm = '';
    if (x.trim().split(' ').length === 1) {
      shortForm = x[0];
    } else {
      const temp = x
        .split(' ')
        .map((name) => name[0])
        .join('')
        .toUpperCase();
      const t = temp.split('');
      shortForm = [t[0], t[t.length - 1]].join('');
    }
    return shortForm;
  },
};
/* eslint-disable */
export default (context, inject) => {
  // const products = (msg) => console.log(`products ${msg}!`)
  // Inject $products(msg) in Vue, context and store.
  inject('global', global);
  // For Nuxt <= 2.12, also add 👇
  // context.$products = products
};
