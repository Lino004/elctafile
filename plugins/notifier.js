export default ({ store }, inject) => {
  inject('notifier', {
    showMessage({ message = '', error = false }) {
      store.commit('module/snackbar/showMessage', { message, error });
    },
  });
};
